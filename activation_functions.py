import numpy as np

def sigmoid(x):
	return 1.0/(1.0+np.exp(-x))

def sigmoid_d(x):
	return sigmoid(x) * (1 - sigmoid(x))

def tanh(x):
	return np.tanh(x)

def tanh_d(x):
	return (1 - (tanh(x)**2))

def relu(x):
	return np.maximum(0.0, x)

def relu_d(x):
	relu_d_x = np.zeros(x.shape)
	relu_d_x[x>0] = 1

	return relu_d_x