from activation import Activation
from input_layer import InputLayer
from fully_connected_layer import FullyConnectedLayer
from linear_output import LinearOutput
from softmax_output import SoftmaxOutput
from neural_network import NeuralNetwork

def build_neural_network():
        input_shape = (None, 28*28)
        layers = [InputLayer(input_shape)]
        layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=150,
                init_stddev=0.1,
                activation_fun=Activation('relu')
        ))
        layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=110,
                init_stddev=0.1,
                activation_fun=Activation('relu')
        ))
        layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=90,
                init_stddev=0.1,
                activation_fun=Activation('relu')
        ))
        layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=75,
                init_stddev=0.1,
                activation_fun=Activation('relu')
        ))
        layers.append(FullyConnectedLayer(
                layers[-1],
                num_units=10,
                init_stddev=0.1,
                activation_fun=None 
        ))
        layers.append(SoftmaxOutput(layers[-1]))

        nn = NeuralNetwork(layers)
        return nn