import numpy as np
import activation_functions as afu

class Activation(object):
    
    def __init__(self, tname):
        if tname == 'sigmoid':
            self.act = afu.sigmoid
            self.act_d = afu.sigmoid_d
        elif tname == 'tanh':
            self.act = afu.tanh
            self.act_d = afu.tanh_d
        elif tname == 'relu':
            self.act = afu.relu
            self.act_d = afu.relu_d
        else:
            raise ValueError('Invalid activation function.')
            
    def fprop(self, input):
        self.last_input = input
        return self.act(input)
    
    def bprop(self, output_grad):
        return output_grad * self.act_d(self.last_input)