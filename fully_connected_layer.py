import numpy as np
from layer import Layer
from parameterized import Parameterized
from activation import Activation

class FullyConnectedLayer(Layer, Parameterized):
    def __init__(self, input_layer, num_units, 
                 init_stddev, activation_fun=Activation('relu')):
        self.num_units = num_units
        self.activation_fun = activation_fun
        # the input shape will be of size (batch_size, num_units_prev) 
        # where num_units_prev is the number of units in the input 
        # (previous) layer
        self.input_shape = input_layer.output_size()
        (batch_size, num_units_prev) = self.input_shape

        self.W = np.random.normal(0, 1, (num_units_prev, num_units)) * init_stddev
        self.b = np.ones(num_units)
        
        self.dW = None
        self.db = None
    
    def output_size(self):
        return (self.input_shape[0], self.num_units)
    
    def fprop(self, input):
        self.last_input = input


        # W = (num_units_prev, num_units)
        # input = (batch_size, num_units_prev)
        # b = (num_units)

        a = np.dot(input, self.W) + self.b
        
        if self.activation_fun == None:
            return a
        
        return self.activation_fun.fprop(a)
        
    def bprop(self, output_grad):
        n = output_grad.shape[0]

        # n = output_grad.shape[0] = batch_size
        # dW = (num_units_prev, num_units)
        # db = (num_units)
        # last_input = (batch_size, num_units_prev)
        # output_grad = (batch_size, num_units)
        # gradient_wrt_net = output_grad = (batch_size, num_units)
        # gradient wrt the input = last_input = (batch_size, num_units_prev)

        if self.activation_fun == None:
            gradient_wrt_net = output_grad
        else:
            gradient_wrt_net = self.activation_fun.bprop(output_grad)

        self.dW = (np.dot(self.last_input.transpose(), gradient_wrt_net))
        self.db = (np.dot(np.ones((1, n)), gradient_wrt_net))

        gradient_wrt_input = (np.dot(gradient_wrt_net, (self.W.transpose())))

        return gradient_wrt_input
        
    def params(self):
        return self.W, self.b

    def grad_params(self):
        return self.dW, self.db