import gradient_checking
import mnist_data_loader
import time
import neural_network_builder

gradient_checking.perform_gradient_checking()

n_train_samples = 10000
(X_train, Y_train, X_valid, Y_valid, X_test, Y_test) = mnist_data_loader.load_mnist_data(n_train_samples)

nn = neural_network_builder.build_neural_network()

t0 = time.time()

nn.train(
    X_train,
    Y_train,
    X_valid,
    Y_valid,
    learning_rate=0.1, 
    max_epochs=50,
    batch_size=20,
    y_one_hot=True,
    descent_type="sgd",
    regularization_rate=1
)

t1 = time.time()
print('Duration: {:.1f}s'.format(t1-t0))

print("Checking test error...")

test_error = nn.classification_error(X_test, Y_test)
print('test error {:.4f}%'.format(test_error * 100))