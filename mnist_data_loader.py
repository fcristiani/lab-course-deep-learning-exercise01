import mnist
import numpy as np

def load_mnist_data(n_train_samples = 10000):
	Dtrain, Dval, Dtest = mnist.load_data()
	X_train, Y_train = Dtrain
	X_valid, Y_valid = Dval
	X_test, Y_test = Dtest

	train_idxs = np.random.permutation(X_train.shape[0])[:n_train_samples]
	X_train = X_train[train_idxs]
	Y_train = Y_train[train_idxs]

	print("X_train shape: {}".format(np.shape(X_train)))
	print("Y_train shape: {}".format(np.shape(Y_train)))

	X_train = X_train.reshape(X_train.shape[0], -1)
	print("Reshaped X_train size: {}".format(X_train.shape))
	X_valid = X_valid.reshape((X_valid.shape[0], -1))
	print("Reshaped X_valid size: {}".format(X_valid.shape))
	X_test = X_test.reshape((X_test.shape[0], -1))
	print("Reshaped X_test size: {}".format(X_test.shape))

	return (X_train, Y_train, X_valid, Y_valid, X_test, Y_test)