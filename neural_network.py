from parameterized import Parameterized
import numpy as np
import one_hot as oh
import scipy.optimize

class NeuralNetwork:
    """ Our Neural Network container class.
    """
    def __init__(self, layers):
        self.layers = layers
        
    def _loss(self, X, Y):
        Y_pred = self.predict(X)

        return self.layers[-1].loss(Y, Y_pred)

    def predict(self, X):
        Y_pred = X
        for layer in self.layers:
            Y_pred = layer.fprop(Y_pred)

        return Y_pred
    
    def backpropagate(self, Y, Y_pred, upto=0):
        next_grad = self.layers[-1].input_grad(Y, Y_pred)

        starting_index_excluding_output_layer = len(self.layers) - 1 - 1
        for l in range(starting_index_excluding_output_layer, upto - 1, -1):
            next_grad = self.layers[l].bprop(next_grad)

        return next_grad
    
    def classification_error(self, X, Y):
        Y_pred = oh.unhot(self.predict(X))
        error = Y_pred != Y
        return np.mean(error)
    
    def sgd_epoch(self, X, Y, learning_rate, batch_size, regularization_rate):
        n_samples = X.shape[0]
        n_batches = n_samples // batch_size

        X_batches = np.array_split(X, n_batches)
        Y_batches = np.array_split(Y, n_batches)

        for b in range(n_batches):
            X_in_batch = X_batches[b]
            Y_in_batch = Y_batches[b]

            self.gd_epoch(X_in_batch, Y_in_batch, learning_rate, regularization_rate)
        pass
    
    def gd_epoch(self, X, Y, learning_rate, regularization_rate):
        batch_size = X.shape[0]

        Y_pred = self.predict(X)
        self.backpropagate(Y, Y_pred)

        for l in range (1, len(self.layers)-1):
            old_W, old_b = self.layers[l].params()
            new_dW, new_db = self.layers[l].grad_params()

            weights_elements = new_dW.shape[0] * new_dW.shape[1]
            bias_elements = len(new_db)
            self.layers[l].W = old_W - learning_rate * ((new_dW) + ((regularization_rate/batch_size) * self.layers[l].W))
            self.layers[l].b = old_b - learning_rate * new_db
        pass
    
    def train(self, X, Y, X_valid, Y_valid, learning_rate=0.1, max_epochs=100, batch_size=64,
              descent_type="sgd", y_one_hot=True, regularization_rate=1):

        """ Train network on the given data. """
        n_samples = X.shape[0]
        n_batches = n_samples // batch_size
        if y_one_hot:
            Y_train = oh.one_hot(Y)
        else:
            Y_train = Y
        print("... starting training")

        for e in range(max_epochs+1):
            if descent_type == "sgd":
                self.sgd_epoch(X, Y_train, learning_rate, batch_size, regularization_rate)
            elif descent_type == "gd":
                self.gd_epoch(X, Y_train, learning_rate, regularization_rate)
            else:
                raise NotImplementedError("Unknown gradient descent type {}".format(descent_type))

            # Output error on the training data
            train_loss = self._loss(X, Y_train)
            train_error = self.classification_error(X, Y)
            print('epoch {:.4f}, loss {:.4f}, train error {:.4f}%'.format(e, train_loss, train_error * 100))

            validation_error = self.classification_error(X_valid, Y_valid)
            print('epoch {:.4f}, validation error {:.4f}%'.format(e, validation_error * 100))
    
    def check_gradients(self, X, Y):
        """ Helper function to test the parameter gradients for
        correctness. """
        for l, layer in enumerate(self.layers):
            if isinstance(layer, Parameterized):
                print('checking gradient for layer {}'.format(l))
                for p, param in enumerate(layer.params()):
                    # we iterate through all parameters
                    param_shape = param.shape
                    # define functions for conveniently swapping
                    # out parameters of this specific layer and 
                    # computing loss and gradient with these 
                    # changed parametrs
                    def output_given_params(param_new):
                        """ A function that will compute the output 
                            of the network given a set of parameters
                        """
                        # copy provided parameters
                        param[:] = np.reshape(param_new, param_shape)
                        # return computed loss

                        return self._loss(X, Y)

                    def grad_given_params(param_new):
                        """A function that will compute the gradient 
                           of the network given a set of parameters
                        """
                        # copy provided parameters
                        param[:] = np.reshape(param_new, param_shape)
                        # Forward propagation through the net
                        Y_pred = self.predict(X)
                        # Backpropagation of partial derivatives
                        self.backpropagate(Y, Y_pred, upto=l)
                        # return the computed gradient 

                        return np.ravel(self.layers[l].grad_params()[p])

                    # let the initial parameters be the ones that
                    # are currently placed in the network and flatten them
                    # to a vector for convenient comparisons, printing etc.
                    param_init = np.ravel(np.copy(param))

                    epsilon = 1e-4

                    err_scipy = scipy.optimize.check_grad(
                        output_given_params, 
                        grad_given_params,
                        param_init
                    )
                    
                    gparam_bprop = grad_given_params(param_init)
                    gparam_fd = self.perform_finite_differences(
                        epsilon,
                        output_given_params,
                        param_init
                    )

                    difference = (gparam_bprop - gparam_fd)
                    err = np.mean(np.abs(gparam_bprop - gparam_fd))

                    assert(err < epsilon)
                    assert(err_scipy < epsilon)
                    
                    # reset the parameters to their initial values
                    param[:] = np.reshape(param_init, param_shape)

    def perform_finite_differences(self, epsilon, f, param_init):
        f_base = f(param_init)
        finite_differences = np.zeros_like(param_init)
        epsilon_i = np.zeros_like(param_init)

        for i in range(len(param_init)):
            epsilon_i[i] = epsilon
            finite_differences[i] = (f(param_init + epsilon_i) - f_base) / epsilon
            epsilon_i[i] = 0

        return finite_differences